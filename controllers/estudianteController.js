const { guardarDB, leerDB } = require('../helpers/guardar');
const Estudiante = require('../models/estudiante');
const Estudiantes = require('../models/estudiantes');

const { response } = 'express';

// instanciamos Personas, lo hacemos global para que se añada un nuevo usuario con cada petición POST
const estudiantes = new Estudiantes();

// Método para obtener una persona
// GET //
const estudianteGet = (req, res = response) => {

  const estudianteDB = leerDB();

  if (estudianteDB) {
    estudiantes.cargarEstudianteFromArray(estudianteDB);
  }

  res.json({
    msg: 'get API - Controlador',
    // Mostramos aqui la lista de personas
    estudianteDB
  })

};

// PUT //
// Método para actulizar una persona
const estudiantePut = (req, res = response) => {

  const { id } = req.params

  if (id) {
    estudiantes.eliminarEstudiante(id)
  
    const { nombres, apellidos, ci, sexo } = req.body;
    // pasamos los parámetros de Persona
    const estudiante = new Estudiante(nombres, apellidos, ci, sexo);
  
    estudiante.setID(id)
  
    estudiantes.crearEstudiante(estudiante);
    guardarDB(estudiantes.listArr);
  }

  res.json({
    msg: 'put API - Controlador'
  })
}

// POST //
// Método para añadir una persona 
const estudiantePost = (req, res = response) => {
  const { nombres, apellidos, ci, sexo } = req.body;

  // pasamos los parámetros de Persona
  const estudiante = new Estudiante(nombres, apellidos, ci, sexo);

  let estudianteDB = leerDB()
  // instanciamos Personas
  // const personas = new Personas();

  // condicion para cargar los datos ya guardados
  if (estudianteDB) {
    estudiantes.cargarEstudianteFromArray(estudianteDB);
  }
  // añadimos una nueva 'persona' a 'personas'
  estudiantes.crearEstudiante(estudiante);
  guardarDB(estudiantes.listArr);
  estudianteDB = leerDB()

  const listado = leerDB();

  if (listado) {
    estudiantes.cargarEstudianteFromArray(listado);
  }

  res.json({
    msg: 'post API - Controlador',
    listado
  })

}

// DELETE //
// Método para eliminar una persona
const estudianteDelete = (req, res = response) => {

  const { id } = req.params
  
  if (id) {
    estudiantes.eliminarEstudiante(id);
    console.log("Este es delete:",estudiantes.listArr);
    guardarDB(estudiantes.listArr)
    // console.log(leerDB());
  }

  res.json({
    msg: 'delete API - Controlador',
  })
};


module.exports = {
  estudianteGet,
  estudiantePut,
  estudiantePost,
  estudianteDelete
}