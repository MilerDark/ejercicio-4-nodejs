const { guardarDB, leerDB } = require('../helpers/guardar');
const Examen = require('../models/examen');
const Examenes = require('../models/examenes');

const { response } = 'express';

// instanciamos Personas, lo hacemos global para que se añada un nuevo usuario con cada petición POST
const examenes = new Examenes();

// Método para obtener una persona
// GET //
const examenGet = (req, res = response) => {

  const examenDB = leerDB();

  if (examenDB) {
    examenes.cargarExamenFromArray(examenDB);
  }

  res.json({
    msg: 'get API - Controlador',
    // Mostramos aqui la lista de personas
    examenDB
  })

};

// PUT //
// Método para actulizar una persona
const examenPut = (req, res = response) => {

  const { id } = req.params

  if (id) {
    examenes.eliminarExamen(id)
  
    const { materia, grado, nota } = req.body;
    // pasamos los parámetros de Persona
    const examen = new Examen(materia, grado, nota);
  
    examen.setID(id)
  
    examenes.crearExamen(examen);
    guardarDB(examenes.listArray);
  }

  res.json({
    msg: 'put API - Controlador'
  })
}

// POST //
// Método para añadir una persona 
const examenPost = (req, res = response) => {
  const { materia, grado, nota } = req.body;

  // pasamos los parámetros de Persona
  const examen = new Examen(materia, grado, nota);

  let examenDB = leerDB()
  // instanciamos Personas
  // const personas = new Personas();

  // condicion para cargar los datos ya guardados
  if (examenDB) {
    examenes.cargarExamenFromArray(examenDB);
  }
  // añadimos una nueva 'persona' a 'personas'
  examenes.crearExamen(examen);
  guardarDB(examenes.listArray);
  examenDB = leerDB()

  const listado1 = leerDB();

  if (listado1) {
    examenes.cargarExamenFromArray(listado1);
  }

  res.json({
    msg: 'post API - Controlador',
    listado1
  })

}

// DELETE //
// Método para eliminar una persona
const examenDelete = (req, res = response) => {

  const { id } = req.params
  
  if (id) {
    examenes.eliminarExamen(id);
    console.log("Este es delete:",examenes.listArray);
    guardarDB(examenes.listArray)
    // console.log(leerDB());
  }

  res.json({
    msg: 'delete API - Controlador',
  })
};


module.exports = {
  examenGet,
  examenPut,
  examenPost,
  examenDelete
}