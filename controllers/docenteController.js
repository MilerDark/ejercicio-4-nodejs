const { guardarDB, leerDB } = require('../helpers/guardar');
const Docente = require('../models/docente');
const Docentes = require('../models/docentes');

const { response } = 'express';

// instanciamos Personas, lo hacemos global para que se añada un nuevo usuario con cada petición POST
const docentes = new Docentes();

// Método para obtener una persona
// GET //
const docenteGet = (req, res = response) => {

  const docenteDB = leerDB();

  if (docenteDB) {
    docentes.cargarDocenteFromArray(docenteDB);
  }

  res.json({
    msg: 'get API - Controlador',
    // Mostramos aqui la lista de personas
    docenteDB
  })

};

// PUT //
// Método para actulizar una persona
const docentePut = (req, res = response) => {

  const { id } = req.params

  if (id) {
    docentes.eliminarDocente(id)
  
    const { nombres, apellidos, ci, sexo, materia } = req.body;
    // pasamos los parámetros de Persona
    const docente = new Docente(nombres, apellidos, ci, sexo, materia);
  
    docente.setID(id)
  
    docentes.crearDocente(docente);
    guardarDB(docentes.listArray2);
  }

  res.json({
    msg: 'put API - Controlador'
  })
}

// POST //
// Método para añadir una persona 
const docentePost = (req, res = response) => {
  const { nombres, apellidos, ci, sexo, materia } = req.body;

  // pasamos los parámetros de Persona
  const docente = new Docente(nombres, apellidos, ci, sexo, materia);

  let docenteDB = leerDB()
  // instanciamos Personas
  // const personas = new Personas();

  // condicion para cargar los datos ya guardados
  if (docenteDB) {
    docentes.cargarDocenteFromArray(docenteDB);
  }
  // añadimos una nueva 'persona' a 'personas'
  docentes.crearDocente(docente);
  guardarDB(docentes.listArray2);
  docenteDB = leerDB()

  const listado2 = leerDB();

  if (listado2) {
    docentes.cargarDocenteFromArray(listado2);
  }

  res.json({
    msg: 'post API - Controlador',
    listado2
  })

}

// DELETE //
// Método para eliminar una persona
const docenteDelete = (req, res = response) => {

  const { id } = req.params
  
  if (id) {
    docentes.eliminarDocente(id);
    console.log("Este es delete:",docentes.listArray2);
    guardarDB(docentes.listArray2)
    // console.log(leerDB());
  }

  res.json({
    msg: 'delete API - Controlador',
  })
};


module.exports = {
  docenteGet,
  docentePut,
  docentePost,
  docenteDelete
}