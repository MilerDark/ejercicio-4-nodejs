const express = require('express');

class Server {
  constructor() {
    this.app = express();
    this.port = process.env.PORT;
    this.estudiantePath = '/api/estudiante';
    this.examenPath = '/api/examen';
    this.docentePath = '/api/docente';
    this.middlewares();
    this.routes();
  }

  // Método para lectura y parseo del body
  middlewares() {
    this.app.use(express.json());
    this.app.use(express.static('public'));
  }

  // Método para las rutas
  routes() {
    this.app.use(this.estudiantePath, require('../routes/estudianteRoutes'));
    this.app.use(this.examenPath, require('../routes/examenRoutes'));
    this.app.use(this.docentePath, require('../routes/docenteRoutes'));
  }

  // Escucha el puerto
  listen() {
    this.app.listen(this.port, () => {
      console.log('Servidor corriendo en el puerto ', this.port);
    });
  }

}


module.exports = Server;