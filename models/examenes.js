const Examen = require("./estudiante");

class Examenes {
  constructor() {
    this._listado1 = [];
  }

  // Método para crear persona
  crearExamen(examen = {}) {
    this._listado1[examen.id] = examen;
  }


  // getter para recorrer los posts de personas y almacenarlos, va junto con crearPersona()
  get listArray() {
    const listado1 = [];
    Object.keys(this._listado1).forEach(key => {
      const examen = this._listado1[key];
      listado1.push(examen);
    })

    return listado1;
  }


  // Método para mostrar la lista de personas en el parámetro GET
  cargarExamenFromArray(examenes = []) {
    examenes.forEach(examen => {
      this._listado1[examen.id] = examen;
    })
  }

  
  // Método para eliminar una persona
  eliminarExamen(id = '') {
    if (this._listado1[id]) {
      // console.log(this._listado[id]);
      delete this._listado1[id];
    }
  }
}

module.exports = Examenes;