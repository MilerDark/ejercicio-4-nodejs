const { v4: uuidv4 } = require('uuid');

class Estudiante {
  id = '';
  nombres = '';
  apellidos = '';
  ci = 0;
  sexo = '';
  id_examen = '';

  constructor(nombres, apellidos, ci, sexo) {
    this.id = uuidv4();
    this.nombres = nombres;
    this.apellidos = apellidos;
    this.ci = ci;
    this.sexo = sexo;
    this.id_examen = uuidv4();
  }

  setID(idx) {
    this.id = idx
  }
}

module.exports = Estudiante;