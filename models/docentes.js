const Docente = require("./docente");

class Docentes {
  constructor() {
    this._listado2 = [];
  }

  // Método para crear 
  crearDocente(docente = {}) {

    this._listado2[docente.id] = docente;
  }


  // getter para recorrer los posts de personas y almacenarlos, va junto con crearPersona()
  get listArray2() {
    const listado2 = [];
    Object.keys(this._listado2).forEach(key => {
      const docente = this._listado2[key];
      listado2.push(docente);
    })

    return listado2;
  }


  // Método para mostrar la lista de personas en el parámetro GET
  cargarDocenteFromArray(docentes = []) {
    docentes.forEach(docente => {
      this._listado2[docente.id] = docente;
    })
  }

  
  // Método para eliminar una persona
  eliminarDocente(id = '') {
    if (this._listado2[id]) {
      // console.log(this._listado[id]);
      delete this._listado2[id];
    }
  }
}

module.exports = Docentes;