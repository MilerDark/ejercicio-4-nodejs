const Estudiante = require("./estudiante");

class Estudiantes {
  constructor() {
    this._listado = [];
  }

  // Método para crear 
  crearEstudiante(estudiante = {}) {

    this._listado[estudiante.id] = estudiante;
  }


  // getter para recorrer los posts de personas y almacenarlos, va junto con crearPersona()
  get listArr() {
    const listado = [];
    Object.keys(this._listado).forEach(key => {
      const estudiante = this._listado[key];
      listado.push(estudiante);
    })

    return listado;
  }


  // Método para mostrar la lista de personas en el parámetro GET
  cargarEstudianteFromArray(estudiantes = []) {
    estudiantes.forEach(estudiante => {
      this._listado[estudiante.id] = estudiante;
    })
  }

  
  // Método para eliminar una persona
  eliminarEstudiante(id = '') {
    if (this._listado[id]) {
      // console.log(this._listado[id]);
      delete this._listado[id];
    }
  }
}

module.exports = Estudiantes;