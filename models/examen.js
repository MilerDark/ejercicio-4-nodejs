const { v4: uuidv4 } = require('uuid');

class Examen {
  id_examen = '';
  materia = '';
  grado = '';
  nota = 0;

  constructor(materia, grado, nota) {
    this.id_examen = uuidv4();
    this.materia = materia;
    this.grado = grado;
    this.nota = nota;
  }

  setID(idx) {
    this.id_examen = idx
  }
}

module.exports = Examen;