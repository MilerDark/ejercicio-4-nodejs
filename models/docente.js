const { v4: uuidv4 } = require('uuid');

class Docente {
  id = '';
  nombres = '';
  apellidos = '';
  ci = 0;
  sexo = '';
  materia = '';

  constructor(nombres, apellidos, ci, sexo, materia) {
    this.id = uuidv4();
    this.nombres = nombres;
    this.apellidos = apellidos;
    this.ci = ci;
    this.sexo = sexo;
    this.materia = materia;
  }

  setID(idx) {
    this.id = idx
  }
}

module.exports = Docente;