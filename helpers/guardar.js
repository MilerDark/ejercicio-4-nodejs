const fs = require('fs');


const archivo = './db/data.json';

// Método para guardar los datos en la DB
const guardarDB = (data) => {
  fs.writeFileSync(archivo, JSON.stringify(data));
  console.log(data);
}

// Método para leer la info de la DB
const leerDB = () => {
  // si no existe el archivo que no retorne nada
  if (!fs.existsSync(archivo)) {
    return null;
  }

  const info = fs.readFileSync(archivo, { encoding: 'utf-8' });
  const data = JSON.parse(info);
  return data;
}


module.exports = {
  guardarDB,
  leerDB
}