const { Router } = require('express');
const { estudianteGet, estudiantePut, estudiantePost, estudianteDelete } = require('../controllers/estudianteController');
const router = Router();

router.get('/', estudianteGet);
router.put('/:id', estudiantePut);
router.post('/', estudiantePost);
router.delete('/:id', estudianteDelete);


module.exports = router;