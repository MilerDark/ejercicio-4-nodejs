const { Router } = require('express');
const { docenteGet, docentePut, docentePost, docenteDelete } = require('../controllers/docenteController');
const router = Router();

router.get('/', docenteGet);
router.put('/:id', docentePut);
router.post('/', docentePost);
router.delete('/:id', docenteDelete);


module.exports = router;