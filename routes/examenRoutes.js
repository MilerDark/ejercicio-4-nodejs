const { Router } = require('express');
const { examenGet, examenPut, examenPost, examenDelete } = require('../controllers/examenController')
const router = Router();

router.get('/', examenGet);
router.put('/:id', examenPut);
router.post('/', examenPost);
router.delete('/:id', examenDelete);

module.exports = router;